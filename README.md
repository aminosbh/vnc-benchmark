# VNC benchmark

This is a benchmark of different free open source VNC projects and libraries.

VNC server/client support is specified per platform (W: Windows, M: Mac OS X, L: Linux, J: Java VM)

## Projects

| Project name              | Server | Client     | Audio | Chat  | Encryption  | Programming language | Source                                         | License  |
| ------------------------- | :----: | :--------: | :---: | :---: | :---------: | :------------------: | :--------------------------------------------: | :------: |
| [RealVNC Open][]          | W/L    | W/L/J      |       |       |             | C, C++, Java         | [zip][RealVNC Open zip]                        | GPLv2    |
| [UltraVNC][]              | W      | W/J        | ✗     | ✓     |             | C, C++, Java         | [zip][UltraVNC zip]                            | GPLv2    |
| [TightVNC][]              | W/L'   | W/M'/L'/J  | ✗     |       |             | C, C++, Java         | [2.8][TightVNC 2.8], [1.3][TightVNC 1.3]       | GPLv2    |
| [TurboVNC][]              | L      | W'/M'/L'/J | ✗     | ✗     | SSL/TLS/SSH | C, C++, Java         | [git][TurboVNC git]                            | GPLv2    |
| [TigerVNC][]              | W/L    | W/M/L/J    | ✗     |       | SSL/TLS     | C, C++, Java         | [git][TigerVNC git]                            | GPLv2    |
| [Vine Server][]           | M      |            |       |       |             | C, C++, Objective-C  | [cvs][Vine Server cvs], [git][Vine Server git] | GPLv3    |
| [Build-in VNC Mac OS X][] | M      |            |       |       |             |                      |                                                |          |
| [Remmina][]               |        | M/L        | ✓     | ✓     | AES-256     | C                    | [git][Remmina git]                             | GPLv2    |
| [Vinagre][]               |        | L          |       |       |             | C                    | [git][Vinagre git]                             | GPLv3    |
| [Vino][]                  | L      |            |       |       |             | C                    | [git][Vino git]                                | GPLv2    |
| [x11vnc][]                | M'/L   |            | ✗     |       | SSL         | C, C++, Tcl          | [git][x11vnc git]                              | GPLv2    |
| [krfb][]                  | L      |            |       |       |             | C++                  | [git][krfb git], [git][krfb github]            | GPLv2    |
| [krdc][]                  |        | L          |       |       |             | C++                  | [git][krdc git], [git][krdc github]            | GPLv2    |
| [Veyon][]                 | W/L    | W/L        | ✗     |       | SSH         | C, C++               | [git][Veyon git]                               | GPLv2    |
| [Chicken][]               |        | M          | ✗     |       |             | C, Objective-C       | [svn][Chicken svn]                             | GPLv2    |
| [Python VNC Viewer][]     |        | ✓          |       |       |             | Python               | [git][Python VNC Viewer git]                   | MIT      |
| [vncdotool][]             |        | ✓          |       |       |             | Python               | [git][vncdotool]                               | MIT      |



[RealVNC Open]: https://archive.realvnc.com/download/open
[RealVNC Open zip]: https://archive.realvnc.com/download/open
[UltraVNC]: https://www.uvnc.com
[UltraVNC zip]: https://www.uvnc.com/component/jdownloads/finish/6-source/376-ultravnc1223src/0.html
[TightVNC]: https://www.tightvnc.com
[TightVNC 2.8]: https://www.tightvnc.com/download.php
[TightVNC 1.3]: https://www.tightvnc.com/download-old.php
[TurboVNC]: https://www.turbovnc.org
[TurboVNC git]: https://github.com/TurboVNC/turbovnc
[TigerVNC]: https://tigervnc.org
[TigerVNC git]: https://github.com/TigerVNC/tigervnc
[Vine Server]: https://sourceforge.net/projects/osxvnc
[Vine Server cvs]: https://sourceforge.net/p/osxvnc/cvs
[Vine Server git]: https://github.com/stweil/OSXvnc
[Build-in VNC Mac OS X]: http://dssw.co.uk/reference/vnc
[Remmina]: https://remmina.org
[Remmina git]: https://gitlab.com/Remmina/Remmina
[Vinagre]: https://wiki.gnome.org/Apps/Vinagre
[Vinagre git]: https://gitlab.gnome.org/GNOME/vinagre
[Vino]: https://wiki.gnome.org/Projects/Vino
[Vino git]: https://gitlab.gnome.org/GNOME/vino
[x11vnc]: http://www.karlrunge.com/x11vnc
[x11vnc git]: https://github.com/LibVNC/x11vnc
[krfb]: https://userbase.kde.org/Krfb
[krfb git]: https://cgit.kde.org/krfb.git/tree
[krfb github]: https://github.com/KDE/krfb
[krdc]: https://docs.kde.org/stable5/en/kdenetwork/krdc/index.html
[krdc git]: https://cgit.kde.org/krdc.git/tree
[krdc github]: https://github.com/KDE/krdc
[Veyon]: https://veyon.io
[Veyon git]: https://github.com/veyon/veyon
[Chicken]: https://sourceforge.net/projects/chicken
[Chicken svn]: https://sourceforge.net/p/chicken/code/HEAD/tree
[Python VNC Viewer]: https://code.google.com/archive/p/python-vnc-viewer
[Python VNC Viewer git]: https://github.com/techtonik/python-vnc-viewer
[vncdotool]: https://github.com/sibson/vncdotool


## Libraries

| Library name              | Server | Client     | Audio | Encryption  | Programming language | Source                                         | License  |
| ------------------------- | :----: | :--------: | :---: | :---------: | :------------------: | :--------------------------------------------: | :------: |
| [LibVNC][]                | ✓      | ✓          |       |             | C, C++, JavaScript   | [git][LibVNC git]                              | GPLv2    |
| [SDL_vnc][]               |        | ✓          |       |             | C                    | [tar.gz][SDL_vnc tar]                          | LGPLv2   |
| [noVNC][]                 |        | ✓          |       |             | Javascript           | [git][noVNC git]                               | MPL      |
| [VncSharp][]              |        | W          |       |             | C#                   | [git][VncSharp]                                | GPL2     |



[LibVNC]: https://libvnc.github.io
[LibVNC git]: https://github.com/LibVNC
[SDL_vnc]: http://www.ferzkopp.net/Software/SDL_vnc
[SDL_vnc tar]: http://www.ferzkopp.net/Software/SDL_vnc/SDL_vnc-1.0.0.tar.gz
[noVNC]: https://novnc.com/info.html
[noVNC git]: https://github.com/novnc/noVNC
[VncSharp]: https://cdot.senecacollege.ca/projects/vncsharp
[VncSharp git]: https://github.com/humphd/VncSharp


## License

Author: Amine B. Hassouna [@aminosbh](https://gitlab.com/aminosbh)

This benchmark is distributed under the terms of the MIT license
[&lt;LICENSE&gt;](LICENSE).
